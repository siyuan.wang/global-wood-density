import numpy as np
import pandas as pd
import os
import argparse
import numpy as np
import pandas as pd
import xgboost as xgb
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.impute import SimpleImputer
from scipy.stats import pearsonr
from pathlib import Path

from autogluon.tabular import TabularDataset, TabularPredictor
from sklearn.ensemble import HistGradientBoostingRegressor
import lightgbm as lgb

def preprocessing(df,features_extend='',exp=False):
    '''
    features_extend: features needed to be maintained for training
    exp: True is to process wood density as exp(WD)-1
    '''
    vars_keep = ["wood_density",
                 "fold",
                 "FAO_ecozones_fao_ecozones_tif",
                 "ESACCI_LC_zcenter",
                 "Koeppen_Kottek_koeppen_nc",
                 "MCD12Q1_zcenter",
                 "leaf_type_habit",
                 "leaf_type",
                 "leaf_habit"]
    with open('./features.txt') as file:
        lines = file.readlines()
        lines = [line.rstrip() for line in lines]

    vars_keep.extend(lines)

    if len(features_extend) != 0:
        vars_keep.extend(features_extend)
    df1 = df.filter(items=vars_keep)
    if exp:
        df1['wood_density'] = np.exp(df1['wood_density'])-1
    return df1,vars_keep

def get_datasets_from_csv(df):

    X_df = df.drop(columns = ["wood_density", "fold"])
    y_df = df[["wood_density"]]

    X_test = X_df[df.fold == 0]
    y_test = y_df[df.fold == 0]
    testset = [X_test, y_test] if len(X_test) > 0 else None

    trainsets = []
    valsets = []
    fold_idxs = []
    for fold in df.fold.unique():            
        if fold != 0:
            X_train = X_df[(df.fold != fold) & (df.fold != 0)]
            y_train = y_df[(df.fold != fold) & (df.fold != 0)]
            X_val = X_df[df.fold == fold]
            y_val = y_df[df.fold == fold]

            if len(X_val) > 2:
                trainsets.append([X_train, y_train])
                valsets.append([X_val, y_val])
                fold_idxs.append(int(fold))

    return fold_idxs, trainsets, valsets, testset

def fit_model(trainset,valset,model = "xgboost"):
    CATEGORICAL = [
    "leaf_type",
    "leaf_type_habit",
    "leaf_habit",
    "FAO_ecozones_fao_ecozones_tif",
    "ESACCI_LC_zcenter",
    "ESACCI_LC_zmode3x3",
    "ESACCI_LC_zmode5x5",
    "Koeppen_Kottek_koeppen_nc",
    "Koeppen_Peel_Koeppen_v20007_3600_1800_nc",
    "MCD12Q1_zcenter",
    "MCD12Q1_zmode3x3",
    "MCD12Q1_zmode5x5",
    "continents_continents",
    # "Geomorphons_x1x1",
    # "Geomorphons_x1x2",
    # "Geomorphons_x1x3",
    # "Geomorphons_x1x4",
    # "Geomorphons_x1x5",
    # "Geomorphons_x2x1",
    # "Geomorphons_x2x2",
    # "Geomorphons_x2x3",
    # "Geomorphons_x2x4",
    # "Geomorphons_x2x5",
    # "Geomorphons_x3x1",
    # "Geomorphons_x3x2",
    # "Geomorphons_x3x3",
    # "Geomorphons_x3x4",
    # "Geomorphons_x3x5",
    # "Geomorphons_x4x1",
    # "Geomorphons_x4x2",
    # "Geomorphons_x4x3",
    # "Geomorphons_x4x4",
    # "Geomorphons_x4x5",
    # "Geomorphons_x5x1",
    # "Geomorphons_x5x2",
    # "Geomorphons_x5x3",
    # "Geomorphons_x5x4",
    # "Geomorphons_x5x5",
    "continents_continents"
    ]
    X_train, y_train = trainset
    X_val, y_val = valset
    
    cat_feats = [c for c in X_train.columns if c in CATEGORICAL]
    cat_feats_idxs = [i for i,c in enumerate(X_train.columns) if c in CATEGORICAL]

    X_train_mean = X_train.mean()
    X_train = X_train.fillna(X_train_mean)
    X_val = X_val.fillna(X_train_mean)

    if model in ["rf", "linear", "ridge", "lasso", "svm", "mlp", "knn", "histgb"]:
        y_train = y_train.values.ravel()

    if model == "xgboost":
        reg = xgb.XGBRegressor(eval_metric = "rmse", verbosity = 0, reg_alpha = 0.0, reg_lambda = 1.0,early_stopping_rounds = 2)
        reg.fit(X_train, y_train,eval_set=[(X_val, y_val)], verbose = False)
    elif model == "rf":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), RandomForestRegressor())
        reg.fit(X_train, y_train)
    elif model == "linear":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), LinearRegression())
        reg.fit(X_train, y_train)
    elif model == "ridge":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), Ridge(alpha = 0.1))
        reg.fit(X_train, y_train)
    elif model == "lasso":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), Lasso(alpha = 0.1, max_iter = 10000))
        reg.fit(X_train, y_train)
    elif model == "knn":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), KNeighborsRegressor(n_neighbors = 10))
        reg.fit(X_train, y_train)
    elif model == "svm":
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), StandardScaler(),SVR())
        reg.fit(X_train, y_train)
    elif model == "histgb":
        reg = HistGradientBoostingRegressor(l2_regularization = 0.01)
        reg.fit(X_train, y_train)
    elif model == "autogluon":
        train_data = TabularDataset(pd.concat([y_train, X_train], axis = 1))
        reg = TabularPredictor(label='wood_density', eval_metric = "r2").fit(train_data, presets='best_quality', time_limit = 2*60)
    elif model == "mlp":
        X_train = X_train.values
        X_val = X_val.values
        reg = make_pipeline(SimpleImputer(missing_values=np.nan, strategy='mean'), StandardScaler(), MLPRegressor(hidden_layer_sizes = [128, 128], random_state=1, max_iter=5000, early_stopping = True, n_iter_no_change = 5, tol = 1e-4))
        reg.fit(X_train, y_train)
    elif model == "lightgbm":
        reg = lgb.LGBMRegressor()
        reg.fit(X_train, y_train,eval_set=[(X_val, y_val)], eval_metric = "rmse", early_stopping_rounds = 2, verbose = False, categorical_feature=cat_feats_idxs)
    elif model == "lightgbm_rf":
        reg = lgb.LGBMRegressor(boosting_type = "rf",subsample=.632,reg_alpha=1, reg_lambda=0,bagging_freq=1)
        reg.fit(X_train, y_train,eval_set=[(X_val, y_val)], eval_metric = "rmse", early_stopping_rounds = 2, verbose = False, categorical_feature=cat_feats_idxs)


    

    #r2_train = reg.score(X_train, y_train)
    #r2_val = reg.score(X_val, y_val)
    # r2_test = reg.score(X_test, y_test)

    y_hat_train = reg.predict(X_train)
    y_hat_val = reg.predict(X_val)
    
    if model in ["rf", "linear", "ridge", "lasso", "svm", "mlp", "knn", "histgb"]:
        r2_train = pearsonr(y_train, y_hat_train)[0]**2
    else:
        r2_train = pearsonr(y_train.values.flatten(), y_hat_train)[0]**2
    r2_val = pearsonr(y_val.values.flatten(), y_hat_val)[0]**2

    if isinstance(y_hat_val, pd.DataFrame):
        y_hat_val = y_hat_val.values.flatten()
     
    if len(y_hat_val.shape) > 1:
        y_hat_val = y_hat_val.flatten()
    
    if isinstance(y_val, pd.DataFrame):
        y_val = y_val.values.flatten()
     
    if len(y_val.shape) > 1:
        y_val = y_val.flatten()
    
    print("r2_train: ",r2_train, " r2_val: ",r2_val)
    return r2_train,r2_val,reg,y_hat_val,y_val

def r2_plot(fold_idxs,r2_ts,r2_vs,outpath):
    import matplotlib.pyplot as plt
    plt.style.use('dark_background')
    fig,ax = plt.subplots(figsize=(10,3))
    folds = [f'Round {i}' for i in range(1, 11)]
    ax.plot(fold_idxs,r2_ts,lw=2,color='Red',label=r'$Train\ \ R^2$')
    ax.plot(fold_idxs,r2_vs,lw=2,color='Blue',label=r'$Validation\ \ R^2$')
    ax.set_ylabel('$R^2$')
    ax.set_ylim(0.0,1)
    ax.legend(loc='lower right')

    props = dict(boxstyle='round', facecolor='wheat', alpha=0.9)
    textstr = r'$Avg\ \ training\ \ R^2 = %.3f$'%(np.mean(r2_ts))
    ax.text(0.02,0.15,textstr,c='k',transform=ax.transAxes,verticalalignment='top', bbox=props)
    plt.grid(True)
    plt.setp(ax.get_xticklabels(), ha="right", rotation=45)
    fig.savefig(outpath,dpi=300,bbox_inches = 'tight')
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('idx', type=int, help='simulation idx')
    args = parser.parse_args()

    # ignore all warning information
    import warnings
    warnings.filterwarnings("ignore")
    #models=["xgboost","linear","rf","knn","mlp","ridge"]
    #cats=["ESACCI","FAO","KK","KP","lat2d","lon5d","random"]
    models = ["xgboost","rf","lightgbm","lightgbm_rf"]
    cats=["FAO_long","KK_new","KK_new_long","SP10"]


    all_inputs = np.array([(x, y) for x in np.arange(1, len(models)+1, 1) for y in np.arange(1, len(cats)+1, 1)]) # here the number is index+1, should minus it.
    input = all_inputs[args.idx]
    model_i =  input[0]
    cat_i = input[1] #category by csv



    cat = cats[cat_i-1]
    model = models[model_i-1]
    
    
    print('ML model is: ',model,' ','Category method is ', cat)
    train_path = Path('./train_selected_features')
    out_path = train_path/cat/model
    out_path.mkdir(parents = True, exist_ok = True)

    csv_paths = cat+'.csv'
    df = pd.read_csv(csv_paths)
    #features_extend = keep_feature_v2(cat)
    df1,vars_keep = preprocessing(df,exp=False)
    fold_idxs, trainsets, valsets, testset = get_datasets_from_csv(df1)
    r2_ts = [] #train
    r2_vs = [] #validation
    fold_text=[]
    for fold, trainset, valset in zip(fold_idxs, trainsets, valsets):
        print("Fold %d is training..." %(fold))
        savefilename = "train_wood_density_cat_%s_model_%s_fold_%d.csv" %(cat,model,fold)
        r2_train,r2_val,reg,y_hat_val,y_val = fit_model(trainset,valset,model)
        pd.DataFrame({'fold_i':y_val,'Prediction for fold_i':y_hat_val}).to_csv(out_path/savefilename)
        r2_ts.append(r2_train)
        r2_vs.append(r2_val)
        fold_text.append(r'Fold %d' %(fold))
        #break
    r2_path = out_path/'r2.png'
    r2_plot(fold_text,r2_ts,r2_vs,r2_path)
    print(f'Done: cat={cat}, model={model}', flush=True)